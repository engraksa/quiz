# Quiz App
* Client interface developed in Reactjs
* Server developed in Node.js and MongoDB

## Install Node dependencies
* `$ cd app`
* `$ npm install`
* `$ cd app-admin`
* `$ npm install`
* `$ cd server`
* `$ npm install`
* `$ cd web-server`
* `$ npm install`

## Run client quiz
* `$ ./start-mongod.sh` require mongodb demon
* `$ cd app`
* `$ npm start`

## Run client quiz admin
* `$ ./start-mongod.sh` require mongodb demon
* `$ cd app-admin`
* `$ npm start`

## Run sever Node
* `$ cd server`
* `$ node index.js`

## Build apps
* `$ cd app`
* `$ npm run build`
* `$ cd app-admin`
* `$ npm run build`

## Install Dependencies via shell
* `$ ./install-dep.sh`
## Build apps via shell
* `$ ./build.sh`
## Build push production via shell
* `$ ./prod.sh`

* then check build sources in `server/build`
* put all production build sources into branch `prod`