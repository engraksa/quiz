const gulp = require('gulp');
const sass = require('gulp-sass');

// *****    build sass to css
let sassPath = './src/sass/';
function build() {
    return gulp.src(sassPath + '*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css/'));
}
gulp.task('sass:build', build);
function watchBuild(cb) {
    const watcher = gulp.watch([sassPath + '**/*.scss']);
    watcher.on('change', build);
}
gulp.task('sass:watch', gulp.series(build, watchBuild));