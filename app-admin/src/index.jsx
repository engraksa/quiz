/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import Confirm from './js/Confirm'
import Waiting from './js/Waiting'
import ModifyQuestion from './js/ModifyQuestion'
import ListQuestion from './js/ListQuestion'
import Home from './js/Home'
import Notify from './js/Notify'
import ModifyCategory from './js/ModifyCategory'
import Profile from './js/Profile'

import ajax from './js/ajax'
import utils from './js/utils'

import './css/index.css'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isWaiting: false,
            answerTypes: [{
                id: utils.answerType.RADIO,
                title: 'single choice'
            }, {
                id: utils.answerType.SELECT,
                title: 'multi choice'
            }],
            catPages: 0,
            catPageInd: 0,
            quizCats: {},
            catIdModifyQuestion: null,
            qIdModifyQuestion: null,
            catIdListQuestion: null,
            catIdModifyCategory: null,
            isAddingCat: false,
            imageFolder: null
        }
    }

    componentDidMount() {
        window.app = this
        this.toggleWaiting(true)
        let url = ajax.getUrl()
        ajax.get(`${url}/quiz/config`, (isOK, data) => {
            this.toggleWaiting(false)
            if (isOK) {
                this.setState(data)
            } else {
                this.notify.error('Retrieve data is not okay!', 'Something wrong')
            }
        })
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                <Confirm
                    ref={(confirm) => { this.confirm = confirm }} />
                <Notify ref={(notify) => {
                    this.notify = notify
                }} />
                {(this.state.catIdModifyQuestion != null ||
                    this.state.qIdModifyQuestion != null) &&
                    <ModifyQuestion
                        answerTypes={this.state.answerTypes}
                        quizCats={this.state.quizCats}
                        imageFolder={this.state.imageFolder}
                        catId={this.state.catIdModifyQuestion}
                        qId={this.state.qIdModifyQuestion}
                        onHomeClick={this._onHomeClick.bind(this)}
                        onListQuestionsClick={this._onQuizCardClick.bind(this)}
                        onAddSuccess={() => { }}
                        toggleWaiting={this.toggleWaiting.bind(this)}
                        notifySuccess={(title, message) => this.notify.success(title, message)}
                        notifyError={(title, message) => this.notify.error(title, message)}
                    />}
                {this.state.catIdListQuestion != null &&
                    <ListQuestion
                        onHomeClick={this._onHomeClick.bind(this)}
                        catId={this.state.catIdListQuestion}
                        toggleWaiting={this.toggleWaiting.bind(this)}
                        onQuestionClick={this._onQuestionClick.bind(this)}
                        onNewQuestionClick={this._onQuestionAddClick.bind(this)}
                        notifySuccess={(title, message) => this.notify.success(title, message)}
                        notifyError={(title, message) => this.notify.error(title, message)}
                    />}
                {(this.state.isAddingCat || this.state.catIdModifyCategory) &&
                    <ModifyCategory
                        catId={this.state.catIdModifyCategory}
                        onHomeClick={this._onHomeClick.bind(this)}
                        onAddSuccess={this._onAddCatSuccess.bind(this)}
                        toggleWaiting={this.toggleWaiting.bind(this)}
                        notifySuccess={(title, message) => this.notify.success(title, message)}
                        notifyError={(title, message) => this.notify.error(title, message)} />}
                {(!this.state.isAddingCat &&
                    this.state.catIdModifyCategory === null &&
                    this.state.catIdListQuestion === null &&
                    this.state.catIdModifyQuestion === null &&
                    this.state.qIdModifyQuestion === null
                ) &&
                    <Home
                        quizCats={this.state.quizCats}
                        onQuizCardClick={this._onQuizCardClick.bind(this)}
                        onQuestionAddClick={this._onQuestionAddClick.bind(this)}
                        onModifyCategoryClick={this._onModifyCategoryClick.bind(this)}
                        onDeleteCategoryClick={this._onDeleteCategoryClick.bind(this)}
                        onAddCatClick={this._onAddCatClick.bind(this)}
                        onQuizCats={this._onQuizCats.bind(this)}
                        toggleWaiting={this.toggleWaiting.bind(this)}
                        notifyError={(title, message) => this.notify.error(title, message)} />}
                <Profile
                    user={this.state.user}
                    onProfileClick={() => { }}
                    style={{
                        position: 'fixed',
                        top: '5px'
                    }} />
                <Waiting isWaiting={this.state.isWaiting} />
            </div>
        )
    }
    _onQuizCats(data) {
        let quizCats = {}
        for (let i = 0; i < data.cats.length; i++)
            quizCats[data.cats[i].id] = data.cats[i]
        this.setState({
            quizCats,
            catPages: data.pages,
            catPageInd: data.pageInd
        }, () => {
            // this._onQuizCardClick(data.cats[0].id)
            // this._onQuestionAddClick(data.cats[0].id)
        })
    }
    _onAddCatSuccess(cat) {
        this.setState(prevState => {
            prevState.quizCats[cat.id] = cat
            return prevState
        })
    }

    _onQuestionClick(id) {
        this._showHome(() => {
            this.setState({ qIdModifyQuestion: id })
        })
    }
    _onModifyCategoryClick(id) {
        this._showHome(() => {
            this.setState({ catIdModifyCategory: id })
        })
    }
    _onDeleteCategoryClick(id) {
        this.confirm.show({
            title: 'Delete Category',
            message: `Category with id ${id} will be deleted by this action!`,
            yesCallback: () => {
                this.toggleWaiting(true)
                let url = ajax.getUrl()
                ajax.post(`${url}/quiz/admin/cat/delete/${id}`, {}, (isOK, response) => {
                    this.toggleWaiting(false)
                    if (isOK) {
                        this.notify.success('Deleting', response)
                        this.setState(prevState => {
                            delete prevState.quizCats[id]
                            return prevState
                        })
                    } else {
                        this.notify.error('Error On Deleting', response)
                    }
                })
            }
        })
    }
    _onAddCatClick() {
        this.setState({ isAddingCat: true })
    }
    _showHome(callback) {
        this.setState({
            isAddingCat: false,
            catIdModifyCategory: null,
            catIdListQuestion: null,
            catIdModifyQuestion: null,
            qIdModifyQuestion: null
        }, callback || function () { })
    }
    _onHomeClick() {
        this._showHome()
    }
    _onQuizCardClick(id) {
        this._showHome(() => this.setState({ catIdListQuestion: id }))
    }
    _onQuestionAddClick(id) {
        this._showHome(() => this.setState({ catIdModifyQuestion: id }))
    }

    toggleWaiting(b) {
        this.setState({ isWaiting: b })
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
