/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

module.exports = {
  admin: {
    id: 1,
    name: 'Admin',
    key: 'admin'
  },
  tester: {
    id: 2,
    name: 'Tester',
    key: 'tester'
  },
  user: {
    id: 3,
    name: 'User',
    key: 'user'
  },
  getRole: function (id) {
    switch (id) {
      case this.admin.id:
        return this.admin
      case this.tester.id:
        return this.tester
      case this.user.id:
        return this.user
      default:
        return null
    }
  },
  getRoleName: function (id) {
    let role = this.getRole(id)
    return role ? role.name : 'Unknown'
  },
  getRoleKey: function (id) {
    let role = this.getRole(id)
    return role ? role.key : 'unknown'
  },
  isAdminRole: function (id) {
    return id === this.admin.id
  },
  isTesterRole: function (id) {
    return id === this.tester.id
  },
  isUserRole: function (id) {
    return id === this.user.id
  }
}