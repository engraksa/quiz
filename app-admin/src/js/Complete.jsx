/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import '../css/Complete.css'
import utils from './utils'

const WIDTH = 50

export default class Complete extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
        this.lastI = 0
        this.pointers = {}
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        this.ensureVisible();
    }

    componentDidUpdate() {
        this.ensureVisible();
    }

    render() {
        return (
            <div id="complete" style={this.props.style || {}}>
                <progress
                    style={{
                        width: `${WIDTH * (this.props.AMOUNT - 1)}px`
                    }}
                    className={`progress-bar${this.props.contrast ? ' contrast' : ''}`}
                    min="1" max="100"
                    value={this.getPercentage()}></progress>
                {this.getAmountElement()}
                <p style={{
                    display: this.props.showInfo ? '' : 'none'
                }} className="info">{this.getPercentage()}% Complete</p>
            </div >
        )
    }

    getPercentage() {
        return Math.floor(this.props.step * 100 / (this.props.AMOUNT - 1))
    }

    getAmountElement() {
        let arr = []
        let props = this.props
        for (let i = 0; i < props.AMOUNT; i++) {
            let isReach = props.step >= i
            if (isReach) this.lastI = i + ''
            arr.push({
                className: isReach ? 'bordered' : '',
                left: `${i * WIDTH}px`
            })
        }
        let background = this.props.pointColor
        this.pointers = {}
        return arr.map((e, ind) => <span
            ref={(ele) => { this.pointers[ind + ''] = ele }}
            key={ind} className={`cursor-pointer point ${e.className}`}
            style={{
                left: e.left,
                background
            }}
            onClick={() => {
                this.props.onStepClick && this.props.onStepClick(ind)
            }} />)
    }

    ensureVisible() {
        let ele = this.pointers[this.lastI]
        ele && ele.scrollIntoView({ inline: 'nearest', block: 'nearest', behavior: 'smooth' })
    }
}