/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
const answerType = {
    RADIO: 1,
    SELECT: 2
}

const utils = {
    INPUT_TEXTAREA_MAX_LENGTH: 50,
    answerType,
    cloneObject(obj) {
        return JSON.parse(JSON.stringify(obj))
    },
    mergeObject(...objects) {
        let result = {}
        objects.forEach((obj) => {
            let newObj = this.cloneObject(obj)
            for (let attr in newObj) result[attr] = newObj[attr]
        })
        return result
    },
    find(arr, func) {
        for (let i = 0; i < arr.length; i++)
            if (func(arr[i], i)) return arr[i]
        return null
    },
    addEvent(object, type, callback) {
        if (object === null || typeof (object) === 'undefined') return
        if (object.addEventListener) {
            object.addEventListener(type, callback, false)
        } else if (object.attachEvent) {
            object.attachEvent("on" + type, callback)
        } else {
            object["on" + type] = callback
        }
    },

    isProduction() {
        return false
    },
    fmtMSS(second) {
        let m = (second / 60 | 0) + ''
        let s = (second % 60) + ''
        return `${m.length > 1 ? m : '0' + m}:${s.length > 1 ? s : '0' + s}`
    },
    encodeUrl(str) {
        return (encodeURIComponent || function (str) {
            str = (str + '')
                .toString();
            // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
            // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
            return encodeURIComponent(str)
                .replace(/!/g, '%21')
                .replace(/'/g, '%27')
                .replace(/\(/g, '%28')
                .replace(/\)/g, '%29')
                .replace(/\*/g, '%2A')
                .replace(/%20/g, '+');
        })(str)
    },
    percent2color(percent) {
        var r, g, b = 0;
        if (percent < 50) {
            r = 255;
            g = Math.round(5.1 * percent);
        } else {
            g = 255;
            r = Math.round(510 - 5.10 * percent);
        }
        var h = r * 0x10000 + g * 0x100 + b * 0x1;
        return '#' + ('000000' + h.toString(16)).slice(-6);
    },
    isDefined(a) {
        return typeof a !== 'undefined'
    },
    strToColorCode(str) {
        if (str) {
            const arrHex = "89ABCDEF"
            for (let i = 0; i < 6 - (str.length < 6 ? str.length : 6); i++) str += 'a'
            var forReturn = ''
            var tmp
            for (let i = 0; i < 3; i++) {
                tmp = str.charCodeAt(i)
                forReturn += arrHex.charAt((tmp % arrHex.length))
            }
            for (let i = 1; i <= 3; i++) {
                tmp = str.charCodeAt(str.length - i)
                forReturn += arrHex.charAt((tmp % arrHex.length))
            }
            return forReturn
        }
        return "FFFFFF";
    },
    getDefaultAvatar() {
        return '/public/image/avatar.png'
    },
}

export default utils