/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

module.exports = {
  local: {
    id: 1,
    name: 'Local',
    key: 'local'
  },
  facebook: {
    id: 2,
    name: 'Facebook',
    key: 'facebook'
  },
  twitter: {
    id: 3,
    name: 'Twitter',
    key: 'twitter'
  },
  googleplus: {
    id: 4,
    name: 'Googleplus',
    key: 'googleplus'
  },
  getAuth: function (id) {
    switch (id) {
      case this.local.id:
        return this.local
      case this.facebook.id:
        return this.facebook
      case this.twitter.id:
        return this.twitter
      case this.googleplus.id:
        return this.googleplus
      default:
        return null
    }
  },
  getAuthName: function (id) {
    let authType = this.getAuth(id)
    return authType ? authType.name : 'Unknown'
  },
  getAuthKey: function (id) {
    let authType = this.getAuth(id)
    return authType ? authType.key : 'unknown'
  },
  isLocalType: function (id) {
    return id === this.local.id
  },
  isFacebookType: function (id) {
    return id === this.facebook.id
  },
  isTwitterType: function (id) {
    return id === this.twitter.id
  },
  isGoogleplusType: function (id) {
    return id === this.googleplus.id
  }
}