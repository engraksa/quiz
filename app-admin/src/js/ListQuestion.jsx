/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import '../css/ListQuestion.css'

import Confirm from './Confirm'
import Complete from './Complete'

import utils from './utils'
import ajax from './ajax'

export default class ListQuestion extends Component {
    constructor(props) {
        super(props)
        this.state = {
            catId: this.props.catId,
            catTitle: '',
            questions: [],
            pages: 1,
            pageInd: 0,
            pattern: '',
            qTotal: 0,
            loading: true
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
        this._path = null
        this._loadQuestions()
    }

    _callForDocumentWidth() {
    }

    _loadQuestions() {
        let url = ajax.getUrl()
        this._path = `/quiz/admin/question?catId=${this.state.catId}&pageInd=${this.state.pageInd}&pattern=${this.state.pattern}`
        ajax.get(url + this._path, (isOK, data) => {
            this.setState({ loading: false }, () => {
                if (data.path !== this._path) return
                if (isOK) {
                    this.setState({
                        catTitle: data.catTitle,
                        questions: data.questions,
                        pages: data.pages,
                        pageInd: data.pageInd,
                        qTotal: data.length
                    })
                } else {
                    this.props.notifyError('Retrieve data is not okay!', 'Something wrong')
                }
            })
        })
    }
    _updateQuestions() {
        this.setState({ loading: true }, this._loadQuestions.bind(this))
    }
    _updateState(option) {
        this.setState(option, this._updateQuestions.bind(this))
    }

    render() {
        return (
            <div>
                <Confirm
                    ref={(confirm) => { this.confirm = confirm }} />
                <div id="list-question">
                    <div className="upper-button">
                        <button className="the-button"
                            onClick={() => {
                                this.props.onHomeClick && this.props.onHomeClick()
                            }}>Home</button>
                        <button className="the-button green"
                            onClick={() => {
                                this.props.onNewQuestionClick && this.props.onNewQuestionClick(this.state.catId)
                            }} >New Question</button>
                    </div>
                    <h1>{this.state.catTitle}</h1>
                    <label>
                        pattern
                        <input type="text" value={this.state.pattern}
                            onChange={(e) => {
                                this.setState({ pattern: e.target.value },
                                    this._updateQuestions.bind(this))
                            }} />
                    </label>
                    , <span>question amount: {`${this.state.questions.length}/${this.state.qTotal}`}</span>
                    <hr />
                    <div className="list-container">
                        {
                            this.state.loading ? <div className="loading">
                                loading
                            </div>
                                : <ul className="ul">
                                    {this.state.questions.map((e, ind) =>
                                        <li className="cursor-pointer" key={ind}
                                            onClick={() => {
                                                this.props.onQuestionClick && this.props.onQuestionClick(e.id)
                                            }}>
                                            <span>{ind}</span>:
                                    <span>{e.text}</span>
                                            <button className="remove-button"
                                                onClick={(event) => {
                                                    event.stopPropagation()
                                                    this._onRemoveClick(ind)
                                                }}>-</button>
                                        </li>)}
                                </ul>}
                    </div>
                    {this.state.pages > 1 &&
                        <div className="pages">
                            <button className="the-button"
                                onClick={this._onPrevClick.bind(this)}
                                disabled={this.state.pageInd === 0}>prev</button>
                            <div className="complete">
                                <Complete
                                    AMOUNT={this.state.pages}
                                    style={{ position: 'relative', opacity: 0.7 }}
                                    step={this.state.pageInd}
                                    pointColor={"#2991d6"}
                                    contrast={true}
                                    showInfo={false}
                                    onStepClick={this._onStepClick.bind(this)} />
                            </div>
                            <button className="the-button"
                                onClick={this._onNextClick.bind(this)}
                                disabled={this.state.pageInd === this.state.pages - 1}>
                                next</button>
                        </div>
                    }
                </div >
            </div>
        )
    }
    _onStepClick(step) {
        this.setState({ pageInd: step }, this._updateQuestions.bind(this))
    }
    _onPrevClick() {
        this.setState(prevState => {
            prevState.pageInd--;
            return prevState
        }, this._updateQuestions.bind(this))
    }
    _onNextClick() {
        this.setState(prevState => {
            prevState.pageInd++;
            return prevState
        }, this._updateQuestions.bind(this))
    }
    _onRemoveClick(ind) {
        let id = this.state.questions[ind].id
        this.confirm.show({
            title: 'Delete Question',
            message: `Question with id ${id} will be deleted by this action!`,
            yesCallback: () => {
                this.props.toggleWaiting(true)
                let url = ajax.getUrl()
                ajax.post(`${url}/quiz/admin/question/delete/${id}`, {}, (isOK, response) => {
                    this.props.toggleWaiting(false)
                    if (isOK) {
                        this.props.notifySuccess && this.props.notifySuccess('Deleting', response)
                        this.setState(prevState => {
                            prevState.questions.splice(ind, 1)
                            return prevState
                        })
                    } else {
                        this.props.notifyError && this.props.notifyError('Error On Deleting', response)
                    }
                })
            }
        })
    }
}