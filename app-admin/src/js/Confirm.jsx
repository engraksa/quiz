/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'

import '../css/Confirm.css'

class Confirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
            title: '',
            message: ''
        }
        this._clearCallback()
    }

    render() {
        return (
            <div style={{
                display: this.state.isShow ? '' : 'none'
            }}>
                <div className="confirm-container">
                    <div className="confirm-body">
                        <h1>{this.state.title}</h1>
                        <h2>{this.state.message}</h2>
                        <br />
                        <h3>Are you okay?</h3>
                        <div className="button-group">
                            <button className="the-button" onClick={this.no.bind(this)}>No</button>
                            <button className="the-button" onClick={this.yes.bind(this)} >Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    yes() {
        this._yesCallback()
        this.hide()
    }
    no() {
        this._noCallback()
        this.hide()
    }
    show(data) {
        this.setState({
            title: data.title,
            message: data.message
        })
        this._noCallback = data.noCallback || function () { }
        this._yesCallback = data.yesCallback || function () { }
        this.setState({
            isShow: true
        })
    }

    _clearCallback() {
        this._noCallback = null
        this._yesCallback = null
    }

    hide() {
        this.setState({
            isShow: false
        })
        this._clearCallback()
    }
}

export default Confirm
