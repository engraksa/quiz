/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import '../css/ModifyCategory.css'

import utils from './utils'
import ajax from './ajax'

const initSate = {
    catId: null,
    title: '',
    description: '',
    quizTime: 30,
    questionAmount: 30,
    bonusTimeScore: 0
}

export default class AddCategory extends Component {
    constructor(props) {
        super(props)
        this.state = utils.cloneObject(initSate)
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        if (this.props.catId !== null) {
            this.props.toggleWaiting(true)
            let url = ajax.getUrl()
            ajax.get(`${url}/quiz/admin/cat/${this.props.catId}`, (isOK, category) => {
                this.props.toggleWaiting(false)
                if (isOK) {
                    category.catId = this.props.catId
                    this.setState(category)
                } else {
                    this.props.notifyError && this.props.notifyError('Error On Retrieving', category)
                }
            })
        }
    }

    render() {
        return (
            <div id="modify-category">
                <div className="upper-button">
                    <button className="the-button"
                        onClick={() => {
                            this.props.onHomeClick && this.props.onHomeClick()
                        }}>
                        Home
                    </button>
                    <button className="the-button green"
                        onClick={this._onNewCategoryClick.bind(this)}
                        disabled={!this._isCatId()} >
                        New Category
                    </button>
                </div>
                <h1>Add Category</h1>
                <hr />
                {this._isCatId() &&
                    <div>
                        <p>category id: {this.state.catId}</p>
                    </div>
                }
                <div className="title-input-container">
                    <textarea className="title-input"
                        maxLength={utils.INPUT_TEXTAREA_MAX_LENGTH * 3}
                        placeholder="Title"
                        value={this.state.title}
                        onChange={e => {
                            this.setState({ title: e.target.value })
                        }}
                    />
                </div>
                <div className="description-input-container">
                    <textarea className="description-input"
                        maxLength={utils.INPUT_TEXTAREA_MAX_LENGTH * 10}
                        placeholder="Description"
                        value={this.state.description}
                        onChange={e => {
                            this.setState({ description: e.target.value })
                        }}
                    />
                </div>
                <div className="question-amount-input-container">
                    <label className="question-amount-input-label">
                        Amount of question per quiz
                        <input type="number" placeholder="0"
                            value={this.state.questionAmount}
                            onChange={(e) => {
                                this.setState({ questionAmount: Number(e.target.value) })
                            }} />
                    </label>
                </div>
                <div className="time-input-container">
                    <label className="time-input-label">
                        Quiz time in second
                        <input type="number" placeholder="0"
                            value={this.state.quizTime}
                            onChange={(e) => {
                                this.setState({ quizTime: Number(e.target.value) })
                            }} />
                    </label>
                </div>
                <div className="time-bonus-score-input-container">
                    <label className="time-bonus-score-input-label">
                        Time bonus score
                        <input type="number" placeholder="0.0"
                            value={this.state.bonusTimeScore}
                            onChange={(e) => {
                                this.setState({ bonusTimeScore: Number(e.target.value) })
                            }} />
                    </label>
                </div>
                <hr />
                <button className="submit-button the-button blue"
                    onClick={this._onSubmit.bind(this)}
                    disabled={!this.state.title || !this.state.description ||
                        !this.state.quizTime || !this.state.questionAmount}>
                    Submit {this._isCatId() ? 'Update' : 'Add'} Category
                </button>
            </div >
        )
    }
    _onNewCategoryClick() {
        this.setState(utils.cloneObject(initSate))
    }
    _isCatId() {
        return this.state.catId !== null
    }
    _onSubmit() {
        this.props.toggleWaiting(true)
        let url = ajax.getUrl()
        let isAdding = !this._isCatId()
        ajax.post(`${url}/quiz/admin/cat/${isAdding ? 'add' : 'update/' + this.state.catId}`, {
            title: this.state.title,
            description: this.state.description,
            quizTime: this.state.quizTime,
            questionAmount: this.state.questionAmount,
            bonusTimeScore: this.state.bonusTimeScore
        }, (isOK, category) => {
            this.props.toggleWaiting(false)
            if (isOK) {
                this.props.onAddSuccess && this.props.onAddSuccess(category)
                this.setState({
                    catId: category.id,
                    title: category.title,
                    description: category.description,
                    quizTime: category.quizTime,
                    questionAmount: category.questionAmount,
                    bonusTimeScore: category.bonusTimeScore
                })
                this.props.notifySuccess && this.props.notifySuccess(isAdding ? 'Adding' : 'Updating',
                    `Category is ${isAdding ? 'added' : 'updated'} successfully`)
            } else {
                this.props.notifyError && this.props.notifyError(`Error On ${isAdding ? 'Adding' : 'Updating'}`, category)
            }
        })
    }
}