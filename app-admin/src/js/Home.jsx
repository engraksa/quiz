/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import '../css/Home.css'
import utils from './utils'
import ajax from './ajax'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentWillReceiveProps(prop) {
    }

    componentDidMount() {
        this.props.toggleWaiting(true)
        let url = ajax.getUrl()
        ajax.get(`${url}/quiz/admin/cat`, (isOK, data) => {
            this.props.toggleWaiting(false)
            if (isOK) {
                this.props.onQuizCats && this.props.onQuizCats(data)
            } else {
                this.props.notifyError('Retrieve data is not okay!', 'Something wrong')
            }
        })
    }

    render() {
        return (
            <div id="home">
                <div className="card-container">
                    <h2 className="title">Quiz Cards</h2>
                    <ul className="flex cards">
                        {this.props.quizCats && this.genQuizCard()}
                    </ul>
                </div>
                <div className="add-cat-button">
                    <button
                        className="the-button"
                        onClick={() => {
                            this.props.onAddCatClick && this.props.onAddCatClick()
                        }}>Add Category
                    </button>
                </div>
            </div >
        )
    }

    genQuizCard() {
        let quizCats = []
        for (let key in this.props.quizCats) quizCats.push(this.props.quizCats[key])
        return quizCats.map((e =>
            <li key={e.id} className="cursor-pointer"
                onClick={() => {
                    this.props.onQuizCardClick && this.props.onQuizCardClick(e.id)
                }}>
                <h2>{e.title}</h2>
                <div className="info">
                    question amount: {e.questionAmount},
                    quiz time: {e.quizTime} seconds,
                    time bonus score: {e.bonusTimeScore}
                </div>
                <hr />
                <p>{e.description}</p>
                <hr />
                <div className="bottom-button">
                    <button className="the-button yellow"
                        onClick={(event) => {
                            event.stopPropagation()
                            this.props.onModifyCategoryClick && this.props.onModifyCategoryClick(e.id)
                        }}>Modify</button>
                    <button className="the-button red"
                        onClick={(event) => {
                            event.stopPropagation()
                            this.props.onDeleteCategoryClick && this.props.onDeleteCategoryClick(e.id)
                        }}>Delete</button>
                    <button className="the-button green"
                        onClick={(event) => {
                            event.stopPropagation()
                            this.props.onQuestionAddClick && this.props.onQuestionAddClick(e.id)
                        }}>Add Question</button>
                </div>
            </li>))
    }

}