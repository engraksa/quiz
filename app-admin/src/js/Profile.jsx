/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import Avatar from './Avatar'
import renderUsername from './renderUsername'
import '../css/Profile.css'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        if(!this.props.user) return null
        return (<div
            className="profile cursor-pointer"
            onClick={() => {
                this.props.onProfileClick && this.props.onProfileClick()
            }}
            style={this.props.style}>
            <div className="avatar-container">
                <Avatar user={this.props.user} />
            </div>
            <div className="user-info">
                <div className="username">
                    {renderUsername(this.props.user.username)}
                </div>
                <div className="profile-info">
                    <span className="quiz-count">{this.props.user.data.quiz || 0}</span>
                    <span className="pass-count">{this.props.user.data.pass || 0}</span>
                    <span className="fail-count">{this.props.user.data.fail || 0}</span>
                    <span className="score-count">{this.props.user.data.score || 0}</span>
                </div>
            </div>
        </div>
        );
    }
}

export default Profile;
