/****************************************************************************
 Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

const ajax = {
    getUrl() {
        return window.SERVER_URL
    },
    get(url, callback = () => {}) {
        let xmlHttp = new XMLHttpRequest()
        xmlHttp.open("GET", url)
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                let data = xmlHttp.responseText
                try {
                    data = JSON.parse(data)
                } catch (e) {}
                callback(xmlHttp.status === 200, data)
            }
        }
        xmlHttp.send()
    },
    post(url, data = {}, callback = () => {}) {
        let xmlHttp = new XMLHttpRequest()
        xmlHttp.open("POST", url)
        xmlHttp.setRequestHeader("Content-Type", "application/json")
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                let data = xmlHttp.responseText
                try {
                    data = JSON.parse(data)
                } catch (e) {}
                callback(xmlHttp.status === 200, data)
            }
        }
        xmlHttp.send(JSON.stringify(data))
    },
    postForm(url, formData, callback = () => {}) {
        let xmlHttp = new XMLHttpRequest()
        xmlHttp.open("POST", url)
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                let data = xmlHttp.responseText
                try {
                    data = JSON.parse(data)
                } catch (e) {}
                callback(xmlHttp.status === 200, data)
            }
        }
        xmlHttp.send(formData)
    }
}

export default ajax