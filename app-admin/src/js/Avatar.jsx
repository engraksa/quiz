/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'

import '../css/Avatar.css'

import utils from './utils'
import authType from './auth-types'

export default class Avatar extends Component {

    render() {
        return (
            <div
                className={`avatar ${this.props.onAvatarClicked ? 'cursor-pointer' : ''}`}
                onClick={this.props.onAvatarClicked && this.props.onAvatarClicked.bind(this)} >
                <img
                    className={`pic ${this.props.user.data.strength}`}
                    src={this.props.user.avatar || utils.getDefaultAvatar()} alt=""
                    onError={(e) => { e.target.src = utils.getDefaultAvatar() }} />
                {this.props.user.authTypeId &&
                    <img
                        className="type"
                        src={`${window.QuizPublicPathAdmin}/raw-image/avatar-${this.getAuthType(this.props.user.authTypeId)}.png`}
                        alt="ava" />
                }
            </div>
        )
    }

    getAuthType(id) {
        switch (id) {
            case authType.facebook.id:
                return 'f';
            case authType.twitter.id:
                return 't';
            case authType.googleplus.id:
                return 'g';
            default:
                return ''
        }
    }
}