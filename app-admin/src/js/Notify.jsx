/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import ReactNotify from 'react-notify'
import '../css/Notify.css'

export default class Notify extends Component {
    constructor(props) {
        super(props)
        this.notifyTime = 6e3
    }

    error(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.error(title, message, notifyTime || this.notifyTime)
    }
    info(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.info(title, message, notifyTime || this.notifyTime)
    }
    success(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.success(title, message, notifyTime || this.notifyTime)
    }

    render() {
        return (
            <div id="notify">
                <ReactNotify
                    ref={(notifier) => { this.notifier = notifier }} />
            </div>
        );
    }
}