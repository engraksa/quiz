/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

import React, { Component } from 'react'
import '../css/ModifyQuestion.css'
import utils from './utils'
import ajax from './ajax'

const answerSample = {
    removed: false,
    text: '',
    isCorrect: false
}
const initState = {
    qId: null,
    answerInputs: [utils.cloneObject(answerSample)],
    text: '',
    correctScore: 0.0,
    incorrectScore: 0.0,
    answerTypeId: utils.answerType.SELECT,
    uploadImage: null,
    imagePreviewUrl: null,
}

export default class ModifyQuestion extends Component {
    constructor(props) {
        super(props)
        this.state = utils.mergeObject(initState, {
            catId: this.props.catId,
        })
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        if (this.props.qId !== null) {
            this.props.toggleWaiting(true)
            let url = ajax.getUrl()
            ajax.get(`${url}/quiz/admin/question/${this.props.qId}`, (isOK, question) => {
                this.props.toggleWaiting(false)
                if (isOK) {
                    let answerInputs = question.answer.options.map(e => ({
                        id: e.id,
                        text: e.text,
                        isCorrect: e.isCorrect,
                        removed: false
                    }))
                    let imagePreviewUrl = null
                    if (question.image)
                        imagePreviewUrl = `${ajax.getUrl()}/${this.props.imageFolder}/${question.image}`
                    this.setState({
                        qId: this.props.qId,
                        catId: question.catId,
                        answerInputs,
                        text: question.text,
                        correctScore: question.correctScore,
                        incorrectScore: question.incorrectScore,
                        answerTypeId: question.answer.type,
                        imagePreviewUrl
                    })
                } else {
                    this.props.notifyError && this.props.notifyError('Error On Retrieving', question)
                }
            })
        }
    }

    render() {
        return (
            <div id="modify-question">
                <div className="upper-button">
                    <button className="the-button"
                        onClick={() => {
                            this.props.onHomeClick && this.props.onHomeClick()
                        }}>
                        Home
                    </button>
                    <button className="the-button"
                        onClick={() => {
                            this.props.onListQuestionsClick && this.props.onListQuestionsClick(this.state.catId)
                        }}>
                        List Questions
                    </button>
                    <button className="the-button green"
                        onClick={this._onNewQuestionClick.bind(this)}
                        disabled={!this._isQId()} >
                        New Question
                    </button>
                </div>
                <h1>{
                    this.props.catId != null ?
                        this.props.quizCats[this.props.catId].title :
                        (this.state.catId != null ? this.props.quizCats[this.state.catId].title : 'Unknown')}
                </h1>
                <hr />
                <div className="container">
                    {this._isQId() &&
                        <div>
                            <p>question id: {this.state.qId}</p>
                        </div>
                    }
                    <div className="score-input-container">
                        <label className="incorrect-score-label">
                            incorrect score
                        <input type="number" placeholder="0.0"
                                value={this.state.incorrectScore}
                                onChange={(e) => {
                                    this.setState({ incorrectScore: Number(e.target.value) })
                                }} />
                        </label>
                        <label className="correct-score-label">
                            correct score
                        <input type="number" placeholder="0.0"
                                value={this.state.correctScore}
                                onChange={(e) => {
                                    this.setState({ correctScore: Number(e.target.value) })
                                }} />
                        </label>
                    </div>
                    <div className="question-input-container">
                        <textarea className="question-input"
                            maxLength={utils.INPUT_TEXTAREA_MAX_LENGTH * 10}
                            placeholder="Question"
                            value={this.state.text}
                            onChange={(e) => {
                                this.setState({ text: e.target.value })
                            }} />
                    </div>
                    <div className="image-input-container">
                        <label className="correct-score-label">
                            image for question
                        <input type="file" accept="image/*"
                                onChange={this._handleImageChange.bind(this)} />
                        </label>
                        {this.state.imagePreviewUrl !== null &&
                            <div className="image-preview">
                                <img src={this.state.imagePreviewUrl} alt="preview" />
                            </div>
                        }
                    </div>
                    <div className="answer-type-input-container">
                        <select className="answer-type-input"
                            value={this.state.answerTypeId}
                            onChange={(e) => {
                                this.setState({ answerTypeId: Number(e.target.value) })
                            }} >
                            {
                                this.props.answerTypes.map(type =>
                                    <option key={type.id} value={type.id}>{type.title}</option>)
                            }
                        </select>
                    </div>
                    <div className="answer-inputs-container">
                        {this._genAnswerInput()}
                        <button className="add-button"
                            onClick={this._onAddAnswerClick.bind(this)} >+</button>
                    </div>
                </div>
                <hr />
                <button className="submit-button the-button blue"
                    onClick={this._onSubmit.bind(this)}
                    disabled={!this._canSubmit()}>
                    Submit {this._isQId() ? 'Update' : 'Add'} Question
                </button>
            </div >
        )
    }
    _handleImageChange(e) {
        e.preventDefault()
        let reader = new FileReader()
        let file = e.target.files[0]
        reader.onloadend = () => {
            this.setState({
                uploadImage: reader.result,
                imagePreviewUrl: reader.result
            });
        }
        reader.readAsDataURL(file)
    }
    _onNewQuestionClick() {
        this.setState(utils.cloneObject(initState))
    }
    _onAddAnswerClick() {
        this.setState(prevState => {
            prevState.answerInputs.push(utils.cloneObject(answerSample))
            return prevState
        })
    }
    _canSubmit() {
        let validAnswerInputs = this._getValidAnswerInputs()
        let blankAnswerTexts = validAnswerInputs.filter(e => !e.text)
        let blankAnswerInCorrects = validAnswerInputs.filter(e => !e.isCorrect)
        let blankAnswerCorrects = validAnswerInputs.filter(e => e.isCorrect)
        return this.state.text && !isNaN(this.state.correctScore) &&
            !isNaN(this.state.incorrectScore) &&
            this.state.correctScore > 0 &&
            this.state.correctScore + this.state.incorrectScore > 0 &&
            !blankAnswerTexts.length &&
            blankAnswerInCorrects.length < validAnswerInputs.length && !(
                this.state.answerTypeId === utils.answerType.RADIO &&
                blankAnswerCorrects.length > 1
            )
    }
    _getValidAnswerInputs() {
        return this.state.answerInputs.filter(e => !e.removed)
    }
    _genAnswerInput() {
        return this.state.answerInputs.map((state, ind) =>
            !state.removed && <div className="answer-input-container" key={ind}>
                <textarea className="answer-input"
                    maxLength={utils.INPUT_TEXTAREA_MAX_LENGTH * 3}
                    placeholder={`Answer ${ind + 1}`}
                    value={state.text}
                    onChange={e => {
                        let text = e.target.value
                        this.setState(prevState => {
                            prevState.answerInputs[ind].text = text
                            return prevState
                        })
                    }} />
                <div className="extension">
                    <label className="right-answer-label">
                        <input type="checkbox"
                            checked={state.isCorrect}
                            onChange={e => {
                                let isCorrect = e.target.checked
                                this.setState(prevState => {
                                    prevState.answerInputs[ind].isCorrect = isCorrect
                                    return prevState
                                })
                            }} />
                        right answer
                    </label>
                    <br />
                    {this._getValidAnswerInputs().length > 1 &&
                        <button className="remove-button" onClick={() => {
                            this.setState(prevState => {
                                prevState.answerInputs[ind].removed = true
                                return prevState
                            })
                        }}>-</button>
                    }
                </div>
            </div>
        )
    }
    _isQId() {
        return this.state.qId !== null
    }
    _onSubmit() {
        let validAnswerInputs = this._getValidAnswerInputs()
        let answers = validAnswerInputs.map((e, ind) => ({
            id: ind,
            text: e.text,
            isCorrect: e.isCorrect
        }))
        this.props.toggleWaiting(true)
        let url = ajax.getUrl()
        let isAdding = !this._isQId()
        let uploadImage
        if (this.state.uploadImage) uploadImage = this.state.uploadImage
        ajax.post(`${url}/quiz/admin/question/${!isAdding ? 'update/' + this.state.qId : 'add'}`,
            {
                catId: this.state.catId,
                text: this.state.text,
                uploadImage,
                correctScore: this.state.correctScore,
                incorrectScore: this.state.incorrectScore,
                answerTypeId: this.state.answerTypeId,
                answers
            }, (isOK, question) => {
                this.props.toggleWaiting(false)
                if (isOK) {
                    this.setState({ qId: question.id, uploadImage: null })
                    this.props.notifySuccess && this.props.notifySuccess(isAdding ? 'Adding' : 'Updating',
                        `Question is ${isAdding ? 'added' : 'updated'} successfully`)
                } else {
                    this.props.notifyError && this.props.notifyError(`Error On ${isAdding ? 'Adding' : 'Updating'}`, question)
                }
            })
    }
}