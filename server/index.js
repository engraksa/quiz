/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const express = require('express')
const path = require('path')

const adminRouter = require('./route/admin-router')
const clientRouter = require('./route/client-router')

const utils = require('./config/utils')
const routeView = require('../web-server/config/route-view')
const route = require('../web-server/controller/routes')

const QuizUserData = require('./model/QuizUserData')

module.exports = function (app) {
  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next()
    res.redirect(routeView.route.AUTH)
  }

  const QuizPublicPath = '/quiz/public'
  const QuizPublicPathAdmin = '/quiz-admin/public'

  app.get(routeView.route.QUIZ, isLoggedIn, (req, res) => {
    res.render(routeView.view.QUIZ, utils.mergeObject(route.genProps(), {
      info: {
        version: '1.0.0',
        versionCode: 100
      },
      QuizPublicPath
    }))
  })
  app.get(routeView.route.QUIZ_ADMIN, isLoggedIn, (req, res) => {
    res.render(routeView.view.QUIZ_ADMIN, utils.mergeObject(route.genProps(), {
      info: {
        version: '1.0.0',
        versionCode: 100
      },
      QuizPublicPathAdmin
    }))
  })

  app.use(QuizPublicPath, express.static(path.join(__dirname, 'public')))
  app.use(QuizPublicPathAdmin, express.static(path.join(__dirname, 'public-admin')))
  app.use('/images', express.static(utils.IMAGE_PATH))

  let views = app.get('views')
  let viewPath = path.join(__dirname, '/views')
  if (views instanceof Array) views.push(viewPath)
  else app.set('views', [viewPath])

  app.get('/quiz/config', (req, res) => {
    var user = req.user.toJson()
    QuizUserData.findByUserId(user.id).then(quizUserData => {
      user.data = quizUserData.forEmit()
      res.status(200).send(JSON.stringify({
        imageFolder: utils.IMAGE_FOLDER,
        user
      }))
    }).catch(e => res.status(505).send('Internal server error'))
  })

  adminRouter.route(app)
  clientRouter.route(app)
}