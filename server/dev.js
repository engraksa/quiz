/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
require('../web-server/db/Database').getInstant()

module.exports = function (port) {
  let customApp = express()
  let limit = {
    limit: '50mb'
  }
  customApp.use(bodyParser.json(limit))
  customApp.use(bodyParser.urlencoded({
    extended: true,
    limit: limit.limit
  }))
  customApp.use(cors())
  customApp.use((req, res, next) => {
    req.user = {
      toJson() {
        return {
          authTypeId: 2,
          avatar: "http://graph.facebook.com/1023468531125799/picture?type=square",
          id: "5a605776ab56939f03370502",
          role: 3,
          username: "Raksa Eng"
        }
      }
    }
    next()
  })
  require('./index')(customApp)
  customApp.listen(port, () => {
    console.log(`Server listening on port ${port}`)
  })
}