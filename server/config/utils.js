/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const path = require('path')
const status = require('./status')

const IMAGE_FOLDER = 'images'
const IMAGE_PATH = path.join(__dirname, '..', IMAGE_FOLDER)

module.exports = {
  PAGE_LIMIT: 10,
  IMAGE_FOLDER,
  IMAGE_PATH,
  responseOK(fn, data) {
    this._response(fn, {
      status: status.OK
    }, data || {})
  },
  responseNotOK(fn, data) {
    this._response(fn, {
      status: status.NOT_OK
    }, data || {})
  },

  getRandomInRang(min, max) {
    if (typeof min == "undefined") min = Number.MAX_VALUE
    if (typeof max == "undefined") {
      max = min
      min = 0
    }
    if (min > max) {
      let temp = min
      min = max
      max = temp
    }
    return Math.floor(min + Math.random() * (max - min + 1))
  },
  getRandomArray(originArr) {
    let arr = [],
      arr1 = originArr.slice(0)
    while (arr1.length) {
      arr.push(arr1.splice(this.getRandomInRang(0, arr1.length - 1), 1)[0])
    }
    return arr
  },
  pickRandom(originArr) {
    if (!originArr.length) return null
    return this.getRandomArray(originArr)[0]
  },

  removeFromArray(arr, item) {
    let index = arr.indexOf(item)
    return ~index && arr.splice(index, 1)
  },
  stringToColour(str) {
    let hash = 0
    for (let i = 0; i < str.length; i++) hash = str.charCodeAt(i) + ((hash << 5) - hash)
    let colour = '#'
    for (let i = 0; i < 3; i++) colour += ('00' + ((hash >> (i * 8)) & 0xFF).toString(16)).substr(-2)
    return colour
  },
  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
  },
  genUUID() {
    let s4 = this.s4
    return s4() + s4()
  },
  mergeObject(...objects) {
    let result = {}
    objects.forEach((obj) => {
      for (let attr in obj) result[attr] = obj[attr]
    })
    return result
  },
  cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj))
  },
  isDefined(a) {
    return typeof a !== 'undefined'
  },
  base64MimeType(encoded) {
    let result = null
    if (typeof encoded !== 'string') return result
    let mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)
    if (mime && mime.length) result = mime[1]
    return result
  }
}