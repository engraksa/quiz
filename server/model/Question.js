/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const camo = require('camo')
const mongodb = require('mongodb')
const Category = require('./Category')
const utils = require('../config/utils')
const path = require('path')
const fs = require('fs')

class Question extends camo.Document {
  constructor() {
    super()

    this.category = Category
    this.text = String
    this.image = String
    this.correctScore = Number
    this.incorrectScore = Number
    this.answerTypeId = Number
    this.answers = [Object]
    this.createdDate = Date
    this.lastUpdatedDate = Date
  }
  setProps(options) {
    if (utils.isDefined(options.text)) this.text = options.text
    if (utils.isDefined(options.image)) this.image = options.image
    if (utils.isDefined(options.correctScore)) this.correctScore = options.correctScore
    if (utils.isDefined(options.incorrectScore)) this.incorrectScore = options.incorrectScore
    if (utils.isDefined(options.answerTypeId)) this.answerTypeId = options.answerTypeId
    if (utils.isDefined(options.answers)) this.answers = options.answers
  }
  deleteImage() {
    if (this.image) {
      try {
        fs.unlinkSync(path.join(utils.IMAGE_PATH, this.image))
      } catch (e) {
        console.error(e)
      }
    }
  }
  preDelete() {
    this.deleteImage()
  }
  preSave() {
    this.lastUpdatedDate = new Date()
  }
  getId() {
    this.idForEmit = this.idForEmit || (this._id ? this._id.toString() : this.genId())
    return this.idForEmit
  }
  genId() {
    return utils.genUUID()
  }
  _forEmit() {
    return {
      id: this.getId(),
      text: this.text,
      image: this.image,
      correctScore: this.correctScore,
      incorrectScore: this.incorrectScore,
    }
  }
  forQuiz() {
    let options = this.answers.map(answer => ({
      id: answer.id,
      text: answer.text
    }))
    return utils.mergeObject(this._forEmit(), {
      answer: {
        type: this.answerTypeId,
        options
      }
    })
  }
  forEmit() {
    return utils.mergeObject(this._forEmit(), {
      answer: {
        type: this.answerTypeId,
        options: this.answers
      }
    })
  }
  calcResult(result) {
    let correctAnswers = this.answers.filter(answer => answer.isCorrect)
    let incorrectAnswers = this.answers.filter(answer => !answer.isCorrect)
    let correctAnswersCount = 0
    correctAnswers.map(answer => {
      if (result.result[answer.id]) correctAnswersCount++
    })
    let incorrectAnswersCount = 0
    incorrectAnswers.map(answer => {
      if (result.result[answer.id]) incorrectAnswersCount++
    })
    let correctPercent = 100 * correctAnswersCount / correctAnswers.length
    let incorrectPercent = 100 * incorrectAnswersCount / incorrectAnswers.length
    let options = this.answers.map(answer => {
      let correct
      if (result.result[answer.id] && answer.isCorrect) correct = true
      return {
        text: answer.text,
        checked: result.result[answer.id],
        correct
      }
    })
    return {
      ind: result.ind,
      question: this._forEmit(),
      correctPercent,
      incorrectPercent,
      submitAnswers: {
        type: this.answerTypeId,
        options
      },
      score: this.correctScore * correctPercent / 100 -
        this.incorrectScore * incorrectPercent / 100
    }
  }
  // *****  Static Method *****

  static collectionName() {
    return 'quiz-question'
  }

  static findInPageInd(category, skip, limit, pattern, callback) {
    callback = callback || function () {}
    Question.find({
      category: category._id,
      text: {
        '$regex': pattern
      }
    }, {
      limit,
      skip
    }).then(arr => callback(arr.map(e => e.forEmit()))).catch(e => {
      callback(null)
    })
  }
  static getLength(category, pattern, callback) {
    callback = callback || function () {}
    Question.find({
      category: category._id,
      text: {
        '$regex': pattern
      }
    }).then(arr => callback(arr.length)).catch(e => callback(null))
  }

  static create(options) {
    options.createdDate = new Date()
    return super.create(options)
  }

  static deleteByCat(options) {
    return new Promise((resolve, reject) => {
      Question.find(options).then(questions => {
        questions.forEach(question => question.delete())
        resolve()
      }).catch(reject)
    })
  }
}
module.exports = Question