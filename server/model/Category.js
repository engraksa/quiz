/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const camo = require('camo')
const utils = require('../config/utils')

class Category extends camo.Document {
  constructor() {
    super()

    this.title = String
    this.description = String
    this.quizTime = Number
    this.questionAmount = Number
    this.bonusTimeScore = Number
    this.createdDate = Date
    this.lastUpdatedDate = Date
  }
  preSave() {
    this.lastUpdatedDate = new Date()
  }
  setProps(options) {
    if (utils.isDefined(options.title)) this.title = options.title
    if (utils.isDefined(options.description)) this.description = options.description
    if (utils.isDefined(options.quizTime)) this.quizTime = options.quizTime
    if (utils.isDefined(options.questionAmount)) this.questionAmount = options.questionAmount
    if (utils.isDefined(options.bonusTimeScore)) this.bonusTimeScore = options.bonusTimeScore
  }
  getId() {
    this.idForEmit = this.idForEmit || (this._id ? this._id.toString() : this.genId())
    return this.idForEmit
  }
  genId() {
    return utils.genUUID()
  }
  forEmit() {
    return {
      id: this.getId(),
      title: this.title,
      description: this.description,
      quizTime: this.quizTime,
      questionAmount: this.questionAmount,
      bonusTimeScore: this.bonusTimeScore
    }
  }
  // *****  Static Method *****

  static collectionName() {
    return 'quiz-category'
  }

  static findInPageInd(skip, limit, pattern, callback) {
    callback = callback || function () {}
    Category.find({
      title: {
        '$regex': pattern
      },
      description: {
        '$regex': pattern
      }
    }, {
      limit,
      skip
    }).then(arr => callback(arr.map(e => e.forEmit()))).catch(e => {
      callback(null)
    })
  }
  static getLength(pattern, callback) {
    callback = callback || function () {}
    Category.find({
      title: {
        '$regex': pattern
      },
      description: {
        '$regex': pattern
      }
    }).then(arr => callback(arr.length)).catch(e => callback(null))
  }

  static create(options) {
    options.createdDate = new Date()
    return super.create(options)
  }
}
module.exports = Category