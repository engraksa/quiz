/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const camo = require('camo')
const User = require('../../web-server/models/User')
const utils = require('../config/utils')

const MAX_SCORE = 2400

class QuizUserData extends camo.Document {
  constructor() {
    super()

    this.owner = User
    this.createdDate = Date
    this.updatedDate = Date
    this.score = Number
    this.quiz = Number
    this.pass = Number
    this.fail = Number
  }

  updateProp(option = {}, callback) {
    callback = callback || function () {}
    if (utils.isDefined(option.score)) this.score = option.score
    if (utils.isDefined(option.quiz)) this.quiz = option.quiz
    if (utils.isDefined(option.pass)) this.pass = option.pass
    if (utils.isDefined(option.fail)) this.fail = option.fail
    this.score = Math.round(this.score)
    let total = this.pass + this.fail
    if (this.quiz < total) this.quiz = total
    return this.save().then(() => {
      callback()
    }).catch(() => {
      callback()
    })
  }

  increaseQuiz() {
    this.quiz = +this.quiz || 0
    this.quiz++;
    return this.updateProp()
  }
  increasePass() {
    this.pass = +this.pass || 0
    this.pass++;
    return this.updateProp()
  }
  increaseFail() {
    this.fail = +this.fail || 0
    this.fail++;
    return this.updateProp()
  }

  addScore(score) {
    if (!+score || score < 0) return null
    this.score = +this.score || 0
    this.score += score
    return this.updateProp()
  }

  preSave() {
    this.updatedDate = new Date()
  }

  getId() {
    return this._id ? this._id.toString() : utils.genUUID()
  }
  forEmit() {
    return {
      strength: this.getStrengthen(),
      score: this.score,
      quiz: this.quiz || 0,
      pass: this.pass || 0,
      fail: this.fail || 0,
    }
  }

  getStrengthen() {
    if (this.score <= 1500) return 'weak'
    if (this.score <= 1800) return 'fair'
    if (this.score <= 2100) return 'medium'
    if (this.score <= MAX_SCORE) return 'strong'
    return 'perfect'
  }

  // *****  Static Method *****

  static collectionName() {
    return 'quiz-user-data'
  }

  static create(options) {
    if (!options.createdDate) options.createdDate = new Date()
    options.score = 0
    options.quiz = 0
    options.pass = 0
    options.fail = 0
    return super.create(options)
  }

  static createFakeData() {
    let quizUserData = this.create({
      createdDate: new Date(),
      updatedDate: new Date(),
      score: Math.ceil(Math.random() * (MAX_SCORE + 100)),
      quiz: 6,
      pass: 4,
      fail: 2,
    })
    // let ooUserData = this.create({
    //   createdDate: new Date(),
    //   updatedDate: new Date(),
    //   score: Math.ceil(Math.random() * (MAX_SCORE + 100)),
    //   quiz: Math.ceil(Math.random() * (MAX_SCORE + 100)),
    //   pass: Math.ceil(Math.random() * (MAX_SCORE + 100)),
    //   fail: Math.ceil(Math.random() * (MAX_SCORE + 100)),
    // })
    quizUserData.setFake(true)
    return quizUserData
  }

  static findByUserId(userId) {
    return new Promise((resolve, reject) => {
      User.findOne({
        _id: userId
      }).then(user => {
        if (user) {
          QuizUserData.findOne({
            owner: user._id
          }).then(quizUserData => {
            if (quizUserData) {
              resolve(quizUserData)
            } else {
              QuizUserData.create({
                owner: user
              }).save().then(quizUserData => {
                if (quizUserData) {
                  resolve(quizUserData)
                } else {
                  reject(new Error('Quiz data can\'t be created'))
                }
              }).create(reject)
            }
          }).catch(reject)
        } else {
          reject(new Error('User not found'))
        }
      }).catch(reject)
    })
  }
  static addData(options, callback) {
    this.findByUserId(options.userId).then(quizUserData => {
      if (utils.isDefined(options.score)) options.score += quizUserData.score
      if (utils.isDefined(options.quiz)) options.quiz += quizUserData.quiz
      if (utils.isDefined(options.pass)) options.pass += quizUserData.pass
      if (utils.isDefined(options.fail)) options.fail += quizUserData.fail
      quizUserData.updateProp(options, callback)
    })
  }
}
module.exports = QuizUserData