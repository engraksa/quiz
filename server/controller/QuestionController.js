/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'
const utils = require('../config/utils')
const base64Img = require('base64-img')
const path = require('path')

const Question = require('../model/Question')
const Category = require('../model/Category')

const UPLOAD_FOLDER = 'upload'
const PAGE_LIMIT = utils.PAGE_LIMIT

function uploadImageToServer(uploadImage, callback) {
  let destPath = path.join(utils.IMAGE_PATH, UPLOAD_FOLDER)
  let d = new Date()
  let date = `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}`
  let namePrefix = 'img-'
  let fileName = `${namePrefix}${utils.genUUID()}-${date}`
  base64Img.img(uploadImage, destPath, fileName, (err, filePath) => {
    if (!err) {
      callback(null, filePath.match(new RegExp(`^.+\\/(${UPLOAD_FOLDER}\\/${namePrefix}.+)$`))[1])
    } else {
      callback(err)
    }
  })
}

class QuestionController {
  index(req, res) {
    function response404() {
      res.status(404).send('data not found')
    }
    let catId = req.query.catId
    let pageInd = req.query.pageInd || 0
    let pattern = req.query.pattern || ''
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(cat => {
        if (cat) {
          Question.getLength(cat, pattern, length => {
            if (length != null) {
              let pages = Math.ceil(length / PAGE_LIMIT)
              if (pages === 0) pages = 1
              if (pageInd >= pages) pageInd = 0
              pageInd = Number(pageInd)
              Question.findInPageInd(cat, pageInd * PAGE_LIMIT, PAGE_LIMIT, pattern,
                questions => {
                  if (questions != null) {
                    res.status(200).send(JSON.stringify({
                      path: req.url,
                      pages,
                      pageInd,
                      questions,
                      catTitle: cat.title,
                      length
                    }))
                  } else {
                    response404()
                  }
                })
            } else {
              response404()
            }
          })
        } else {
          response404()
        }
      })
    } else {
      response404()
    }
  }
  getQuestion(req, res) {
    function response404() {
      res.status(404).send('question not found')
    }
    let qId = req.params.id
    if (utils.isDefined(qId)) {
      Question.findOne({
        _id: qId
      }).then(question => {
        if (question) {
          res.status(200).send(JSON.stringify(utils.mergeObject(question.forEmit(), {
            catId: question.category._id.toString()
          })))
        } else {
          response404()
        }
      }).catch(e => response404)
    } else {
      response404()
    }
  }
  add(req, res) {
    function response505() {
      res.status(505).send('question can\'t be saved')
    }
    let {
      catId,
      text,
      correctScore,
      incorrectScore,
      answerTypeId,
      answers,
      uploadImage
    } = req.body
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(cat => {
        if (cat) {
          let createQuestion = (image) => {
            let question = Question.create({
              category: cat,
              image,
              text,
              incorrectScore,
              correctScore,
              answerTypeId,
              answers
            })
            question.save().then(question =>
                res.status(200).send(JSON.stringify(question.forEmit())))
              .catch(e => response505)
          }
          if (utils.isDefined(uploadImage)) {
            uploadImageToServer(uploadImage, (err, imagePath) => {
              if (!err) createQuestion(imagePath)
              else response505()
            })
          } else createQuestion()
        } else {
          response505()
        }
      })
    } else {
      response505()
    }
  }
  delete(req, res) {
    function response505() {
      res.status(505).send('question can\'t be deleted')
    }
    let qId = req.params.id
    if (utils.isDefined(qId)) {
      Question.deleteOne({
        _id: qId
      }).then(() => {
        res.status(200).send('question is deleted')
      }).catch(e => response505)
    } else {
      response505()
    }
  }
  update(req, res) {
    function response505() {
      res.status(505).send('question can\'t be updated')
    }
    let qId = req.params.id
    if (utils.isDefined(qId)) {
      Question.findOne({
        _id: qId
      }).then(question => {
        if (question) {
          let {
            text,
            correctScore,
            incorrectScore,
            answerTypeId,
            answers,
            uploadImage
          } = req.body
          let updateQuestion = (image) => {
            question.setProps({
              text,
              image,
              correctScore,
              incorrectScore,
              answerTypeId,
              answers
            })
            question.save().then(question =>
                res.status(200).send(JSON.stringify(question.forEmit())))
              .catch(e => response505)
          }
          if (utils.isDefined(uploadImage)) {
            question.deleteImage()
            uploadImageToServer(uploadImage, (err, imagePath) => {
              if (!err) updateQuestion(imagePath)
              else response505()
            })
          } else updateQuestion()
        } else {
          response505()
        }
      }).catch(e => response505)
    } else {
      response505()
    }
  }
}

module.exports = QuestionController