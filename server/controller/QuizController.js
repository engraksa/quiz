/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const utils = require('../config/utils')
const Category = require('../model/Category')
const Question = require('../model/Question')
const QuizUserData = require('../model/QuizUserData')

const PAGE_LIMIT = utils.PAGE_LIMIT

class QuizController {
  index(req, res) {
    function response404() {
      res.status(404).send('data not found')
    }
    let catId = req.params.id
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(cat => {
        if (cat) {
          Question.find({
            category: cat._id
          }).then(arr => {
            if (arr.length) {
              let catForEmit = cat.forEmit()
              arr = utils.getRandomArray(arr).filter((a, ind) => ind < catForEmit.questionAmount)
              let questions = arr.map(a => a.forQuiz())
              res.status(200).send(JSON.stringify({
                catId: catForEmit.id,
                catTitle: catForEmit.title,
                quizTime: catForEmit.quizTime,
                questions
              }))
            } else {
              response404()
            }
          }).catch(response404)
        } else {
          response404()
        }
      }).catch(e => response404)
    } else {
      response404()
    }
  }
  getCats(req, res) {
    function response404() {
      res.status(404).send('result not found')
    }
    let pageInd = req.query.pageInd || 0
    let pattern = req.query.pattern || ''
    Category.getLength(pattern, length => {
      if (length != null) {
        let pages = Math.ceil(length / PAGE_LIMIT)
        if (pages === 0) pages = 1
        if (pageInd >= pages) pageInd = 0
        pageInd = Number(pageInd)
        Category.findInPageInd(pageInd * PAGE_LIMIT, PAGE_LIMIT, pattern,
          cats => {
            if (cats != null) {
              res.status(200).send(JSON.stringify({
                pages,
                pageInd,
                cats
              }))
            } else {
              response404()
            }
          })
      } else {
        response404()
      }
    })
  }
  postIndex(req, res) {
    res.status(200).send(JSON.stringify({}))
  }
  submit(req, res) {
    function response505() {
      res.status(505).send('something wrong on server')
    }
    var clientResult = req.body.result
    if (utils.isDefined(clientResult)) {
      Category.findOne({
        _id: clientResult.catId
      }).then(category => {
        if (category) {
          var cat = category.forEmit()
          let submitResult = clientResult.submitResult
          var submitResultHash = {}
          submitResult.map(result => submitResultHash[result.qId] = result)
          Question.find({
            _id: {
              '$in': submitResult.map(obj => obj.qId)
            }
          }).then(questions => {
            let result = questions
              .map(question => question.calcResult(submitResultHash[question.getId()]))
              .sort((q1, q2) => q1.ind > q2.ind)
            let score = Math.round(result.reduce((r, e) => r + e.score, 0))
            let maxScore = questions.reduce((r, q) => r + q.correctScore, 0)
            let isPass = score / maxScore > 0.5
            let timeBonusScore = cat.bonusTimeScore
            timeBonusScore = Math.round(clientResult.time / clientResult.quizTime * timeBonusScore)
            QuizUserData.addData({
              userId: req.user.toJson().id,
              score: score + timeBonusScore,
              quiz: 1,
              pass: isPass ? 1 : 0,
              fail: isPass ? 0 : 1,
            }, () => {
              res.status(200).send(JSON.stringify({
                catId: cat.id,
                catTitle: cat.title,
                statusOK: true,
                timeBonusScore,
                time: clientResult.time,
                quizTime: clientResult.quizTime,
                score,
                isPass,
                result
              }))
            })
          }).catch(e => response505)
        } else {
          response505()
        }
      }).catch(e => response505)
    } else {
      response505()
    }
  }
}

module.exports = QuizController