/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const utils = require('../config/utils')
const Category = require('../model/Category')
const Question = require('../model/Question')

const PAGE_LIMIT = utils.PAGE_LIMIT

class CategoryController {
  index(req, res) {
    function response404() {
      res.status(404).send('result not found')
    }
    let pageInd = req.query.pageInd || 0
    let pattern = req.query.pattern || ''
    Category.getLength(pattern, length => {
      if (length != null) {
        let pages = Math.ceil(length / PAGE_LIMIT)
        if (pages === 0) pages = 1
        if (pageInd >= pages) pageInd = 0
        pageInd = Number(pageInd)
        Category.findInPageInd(pageInd * PAGE_LIMIT, PAGE_LIMIT, pattern,
          cats => {
            if (cats != null) {
              res.status(200).send(JSON.stringify({
                pages,
                pageInd,
                cats
              }))
            } else {
              response404()
            }
          })
      } else {
        response404()
      }
    })
  }
  getCat(req, res) {
    function response404() {
      res.status(404).send('category not found')
    }
    let catId = req.params.id
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(category => {
        if (category) {
          res.status(200).send(JSON.stringify(utils.mergeObject(category.forEmit())))
        } else {
          response404()
        }
      }).catch(e => response404)
    } else {
      response404()
    }
  }
  add(req, res) {
    function response505() {
      res.status(505).send('category can\'t be save')
    }
    let {
      title,
      description,
      quizTime,
      bonusTimeScore,
      questionAmount
    } = req.body
    if (title && description) {
      let cat = Category.create({
        title,
        description,
        quizTime,
        bonusTimeScore,
        questionAmount
      })
      cat.save().then(cat => res.status(200).send(JSON.stringify(cat.forEmit())))
        .catch(e => response505)
    } else {
      response505()
    }
  }
  delete(req, res) {
    function response505() {
      res.status(505).send('category can\'t be deleted')
    }
    let catId = req.params.id
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(cat => {
        if (cat) {
          Question.deleteByCat({
            category: cat._id
          }).then(() => {
            Category.deleteOne({
              _id: catId
            }).then(() => {
              res.status(200).send('category is deleted')
            }).catch(e => response505)
          }).catch(e => response505)
        } else {
          response505()
        }
      })
    } else {
      response505()
    }
  }
  update(req, res) {
    function response505() {
      res.status(505).send('category can\'t be updated')
    }
    let catId = req.params.id
    if (utils.isDefined(catId)) {
      Category.findOne({
        _id: catId
      }).then(category => {
        if (category) {
          let {
            title,
            description,
            quizTime,
            bonusTimeScore,
            questionAmount
          } = req.body
          category.setProps({
            title,
            description,
            quizTime,
            bonusTimeScore,
            questionAmount
          })
          category.save().then(category =>
              res.status(200).send(JSON.stringify(category.forEmit())))
            .catch(e => response505)
        } else {
          response505()
        }
      }).catch(e => response505)
    } else {
      response505()
    }
  }
}

module.exports = CategoryController