import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import QuizBox from './js/QuizBox'
// import QuizBox1 from './js/QuizBox1'
import Complete from './js/Complete'
import Timer from './js/Timer'
import Waiting from './js/Waiting'
import ScoreBack from './js/ScoreBack'
import Notify from './js/Notify'
import Home from './js/Home'
import Profile from './js/Profile'

import ajax from './js/ajax'

import './css/index.css'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isWaiting: false,
            quizData: null,
            result: null,
            scoreBack: null,
            step: 0,
            isQuizzing: false,
            quizCats: [],
            imageFolder: ''
        }
        this.timer = null
    }

    componentDidMount() {
        window.app = this
        // setTimeout(() => {
        // }, 2e3)
        this.getConfig()
    }

    componentWillUnmount() {
    }

    getConfig() {
        this.toggleWaiting(true)
        let url = ajax.getUrl()
        ajax.get(`${url}/quiz/config`, (isOK, data) => {
            this.toggleWaiting(false)
            if (isOK) {
                this.setState(data)
            } else {
                this.notify.error('Retrieve Config Data', 'Something wrong white retrieving data!')
            }
        })
    }

    render() {
        return (
            <div>
                <Notify ref={(notify) => {
                    this.notify = notify
                }} />
                {this.state.isQuizzing ?
                    <div>
                        <Timer ref={(timer) => { this.timer = timer }}
                            onTimeout={this._onTimeout.bind(this)} />
                        {
                            this.state.quizData && <div id="quiz-box-container">
                                <h1 id="quiz-title">{this.state.quizData.catTitle}</h1>
                                <hr />
                                {this.state.quizData && this.state.quizData.questions.length > 1 &&
                                    <div id="complete-container">
                                        <Complete
                                            AMOUNT={this.state.quizData.questions.length}
                                            style={{
                                                position: 'relative',
                                            }}
                                            step={this.state.step}
                                            contrast={true}
                                            showInfo={false}
                                            onStepClick={this._onStepClick.bind(this)} />
                                    </div>
                                }
                                <QuizBox
                                    step={this.state.step}
                                    questions={this.state.quizData.questions}
                                    isCanPrev={this._isCanPrev()}
                                    isCanNext={this._isCanNext()}
                                    isCanSubmit={this._isCanSubmit()}
                                    onPrev={this._onPrevClick.bind(this)}
                                    onNext={this._onNextClick.bind(this)}
                                    onSubmit={this._onSubmit.bind(this)}
                                    onAnswer={this._onAnswer.bind(this)}
                                    imageFolder={this.state.imageFolder}
                                    onImageError={this._onImageError.bind(this)}
                                    onImageLoad={this._onImageLoad.bind(this)} />
                            </div>
                        }
                    </div> : <Home
                        onQuizCardClick={this._onQuizCardClick.bind(this)}
                        toggleWaiting={this.toggleWaiting.bind(this)}
                        notifyError={(title, message) => this.notify.error(title, message)} />
                }
                {this.state.scoreBack && this.state.scoreBack.statusOK &&
                    <ScoreBack
                        timeBonusScore={this.state.scoreBack.timeBonusScore}
                        score={this.state.scoreBack.score}
                        isPass={this.state.scoreBack.isPass}
                        time={this.state.scoreBack.time}
                        MAX_TIME={this.state.scoreBack.MAX_TIME}
                        result={this.state.scoreBack.result}
                        onHome={this._onHomeClick.bind(this)}
                        nextQuiz={this._onNextQuiz.bind(this)}
                        imageFolder={this.state.imageFolder} />}
                <Profile
                    user={this.state.user}
                    onProfileClick={() => { }}
                    style={{
                        position: 'fixed',
                        top: '5px'
                    }} />
                <Waiting isWaiting={this.state.isWaiting} />
                {/* <QuizBox1 /> */}
            </div>
        )
    }
    _showHome(callback) {
        this.setState({
            scoreBack: null,
            isQuizzing: false,
        }, callback || function () { })
    }
    newQuiz(catId) {
        if (typeof catId === "undefined") catId = this.oldQuizCatId
        this.oldQuizCatId = catId
        if (typeof catId === "undefined") {
            this.notify.error('No Id', 'No category Id provided')
            return
        }
        this.toggleWaiting(true)
        let url = ajax.getUrl()
        ajax.get(`${url}/quiz/client/${catId}`, (isOK, data) => {
            this.toggleWaiting(false)
            if (isOK) {
                let result = {}
                data.questions.forEach((question, ind) => {
                    result[question.id] = {
                        qId: question.id,
                        result: {}
                    }
                })
                this.setState({ quizData: data, isQuizzing: true, result, step: 0 }, () => {
                    this.timer.startTimer(this.state.quizData.quizTime)
                    // setTimeout(() => {
                    //     this._onSubmit()
                    // }, 2e3)
                })
            } else {
                this.notify.error('Retrieve data is not okay!', 'Something wrong')
            }
        })
    }
    _onHomeClick() {
        this._showHome()
    }
    _onQuizCardClick(id) {
        this.newQuiz(id)
    }
    _onNextQuiz() {
        this._showHome(() => {
            this.newQuiz()
        })
    }
    _onImageError(qId) {
        // console.log('image load error ', qId)
    }
    _onImageLoad(qId) {
        // console.log('image load success ', qId)
    }

    _onTimeout() {
        this.notify.error('Quiz Timeout!', 'Your instance time is gone, auto submitting has been invoked')
        this._onSubmit()
    }
    _isCanPrev() {
        return this.state.step > 0
    }
    _isCanNext() {
        return this.state.step < this.state.quizData.questions.length - 1
    }
    _isCanSubmit() {
        return this.state.step >= this.state.quizData.questions.length - 1
    }
    _onPrevClick() {
        if (this._isCanPrev()) {
            this.toggleWaiting(true)
            this.setState(prevState => ({ step: prevState.step - 1 }), () => {
                this.toggleWaiting(false)
            })
        }
    }
    _onNextClick() {
        if (this._isCanNext()) {
            this.toggleWaiting(true)
            this.setState(prevState => ({ step: prevState.step + 1 }), () => {
                this.toggleWaiting(false)
            })
        }
    }
    _onStepClick(step) {
        this.setState({ step })
    }
    _onAnswer(data) {
        this.setState(prevState => {
            prevState.result[data.qId] = data
            return prevState
        })
    }
    _onSubmit() {
        this.toggleWaiting(true)
        this.timer.stopTimer()
        let submitResult = []
        for (let key in this.state.result) {
            submitResult.push(this.state.result[key])
        }
        submitResult.sort((result1, result2) => result1.ind > result2.ind)
        let result = {
            catId: this.state.quizData.catId,
            submitResult,
            quizTime: this.timer.getMaxTime(),
            time: this.timer.getTime()
        }
        this.setState({ quizData: null }, () => {
            let url = ajax.getUrl()
            ajax.post(`${url}/quiz/client/submit`, { result }, (isOK, scoreBack) => {
                this.toggleWaiting(false)
                // console.log('result ', result)
                // console.log('data ', data)
                if (isOK) {
                    this.setState({ scoreBack })
                    this.getConfig()
                } else {
                    this.notify.error('Submitting', 'Error occur on submitting')
                    this._showHome()
                }
            })
        })
    }

    toggleWaiting(b) {
        this.setState({ isWaiting: b })
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
