module.exports = {
  local: {
    id: 1,
    name: 'Local',
    key: 'local'
  },
  facebook: {
    id: 2,
    name: 'Facebook',
    key: 'facebook'
  },
  twitter: {
    id: 3,
    name: 'Twitter',
    key: 'twitter'
  },
  googleplus: {
    id: 4,
    name: 'Googleplus',
    key: 'googleplus'
  },
  getAuth: function (id) {
    switch (id) {
      case this.local.id:
        return this.local
      case this.facebook.id:
        return this.facebook
      case this.twitter.id:
        return this.twitter
      case this.googleplus.id:
        return this.googleplus
      default:
        return null
    }
  },
  getAuthName: function (id) {
    let authType = this.getAuth(id)
    return authType ? authType.name : 'Unknown'
  },
  getAuthKey: function (id) {
    let authType = this.getAuth(id)
    return authType ? authType.key : 'unknown'
  },
  isLocalType: function (id) {
    return id === this.local.id
  },
  isFacebookType: function (id) {
    return id === this.facebook.id
  },
  isTwitterType: function (id) {
    return id === this.twitter.id
  },
  isGoogleplusType: function (id) {
    return id === this.googleplus.id
  }
}