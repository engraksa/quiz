import React, { Component } from 'react'
import Question from './Question'
import '../css/QuizBox.css'
import utils from './utils'

export default class QuizBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            question: null,
            answer: null
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentWillReceiveProps(prop) {
        this.setState({
            question: prop.questions[prop.step].q,
            answer: prop.questions[prop.step].a
        })
    }

    render() {
        return (
            <div id="quiz-box">
                <div className="buttons">
                    <button onClick={() => {
                        this.props.onPrev && this.props.onPrev()
                    }} className="the-button" disabled={!this.props.isCanPrev}>
                        Prev
                    </button>
                    <button onClick={() => {
                        this.props.onNext && this.props.onNext()
                    }} className="the-button" disabled={!this.props.isCanNext}>
                        Next
                    </button>
                </div>
                <hr />
                {
                    this.props.questions.map((question, ind) =>
                        <Question key={ind}
                            id={question.id}
                            text={question.text}
                            correctScore={question.correctScore}
                            incorrectScore={question.incorrectScore}
                            imageFolder={this.props.imageFolder}
                            image={question.image}
                            answer={question.answer}
                            ind={ind}
                            step={this.props.step}
                            onAnswer={this.props.onAnswer}
                            onImageError={this.props.onImageError}
                            onImageLoad={this.props.onImageLoad} />)
                }
                <hr />
                <div className="buttons">
                    <button onClick={() => {
                        this.props.onSubmit && this.props.onSubmit()
                    }} className="the-button" disabled={!this.props.isCanSubmit}>
                        Submit
                    </button>
                </div>
            </div >
        )
    }

}