import React, { Component } from 'react'
import '../css/QuizBox1.css'
import utils from './utils'

export default class QuizBox1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    render() {
        return (
            <div id="quiz-box1">
                <div className="control-group">
                    <h1>Checkboxes</h1>
                    <label className="control control--checkbox">First checkbox
                        <input type="checkbox" defaultChecked="checked" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--checkbox">Second checkbox
                        <input type="checkbox" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--checkbox">Disabled
                        <input type="checkbox" disabled="disabled" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--checkbox">Disabled & checked
                        <input type="checkbox" disabled="disabled" defaultChecked="checked" />
                        <div className="control__indicator"></div>
                    </label>
                </div>
                <div className="control-group">
                    <h1>Radio buttons</h1>
                    <label className="control control--radio">First radio
                        <input type="radio" name="radio" defaultChecked="checked" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--radio">Second radio
                        <input type="radio" name="radio" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--radio">Disabled
                        <input type="radio" name="radio2" disabled="disabled" />
                        <div className="control__indicator"></div>
                    </label>
                    <label className="control control--radio">Disabled & checked
                        <input type="radio" name="radio2" disabled="disabled" defaultChecked="checked" />
                        <div className="control__indicator"></div>
                    </label>
                </div>
                <div className="control-group">
                    <h1>Select boxes</h1>
                    <div className="select">
                        <select>
                            <option>First select</option>
                            <option>Option</option>
                            <option>Option</option>
                        </select>
                        <div className="select__arrow"></div>
                    </div>
                    <div className="select">
                        <select>
                            <option>Second select</option>
                            <option>Option</option>
                            <option>Option</option>
                        </select>
                        <div className="select__arrow"></div>
                    </div>
                    <div className="select">
                        <select disabled="disabled">
                            <option>Disabled</option>
                            <option>Option</option>
                            <option>Option</option>
                        </select>
                        <div className="select__arrow"></div>
                    </div>
                </div>
            </div >
        )
    }
}