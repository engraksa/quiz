import React, { Component } from 'react'
import '../css/Timer.css'
import utils from './utils'

const MAX_DEG = 290

export default class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            MAX_TIME: null,
            time: null
        }

        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        window.timer = this
    }
    componentWillUnmount() {
        this.pauseTimer()
    }

    startTimer(time) {
        this.pauseTimer()
        time = time || this.state.MAX_TIME || 20
        this.setState({ MAX_TIME: time, time }, () => {
            this.resumeTimer()
        })
    }
    resumeTimer() {
        this.interval = setInterval(() => {
            this.setState(preState => {
                preState.time -= this.isPause ? 0 : 1;
                return preState
            }, () => {
                if (this.state.time <= 0) {
                    this.pauseTimer()
                    this.props.onTimeout && this.props.onTimeout()
                }
            })
        }, 1e3)
    }
    stopTimer() {
        this.pauseTimer()
        this.setState({ time: 20 })
    }
    pauseTimer() {
        this.interval && clearInterval(this.interval)
    }
    getTime() {
        return this.state.time
    }
    getMaxTime() {
        return this.state.MAX_TIME
    }

    render() {
        return (
            <div id="timer">
                <div className="ring-wrap">
                    <svg className="c-t" width="100" height="100">
                        <circle className={`c-c${this.getPercent() <= 0.25 ? ' warning' : ''}`}
                            style={{
                                strokeDashoffset: this.getStrokeDash(),
                                stroke: this.getStrokeColor()
                            }}
                            cx="50" cy="50" r="45" transform="rotate(-90, 50, 50)" />
                        <circle className="c-c1"
                            cx="50" cy="50" r="42" transform="rotate(-90, 50, 50)" />
                    </svg>
                </div>
                <div className="timer-info">
                    <div className="t-time">
                        {this.getTimeState()}
                    </div>
                    <div className="oop">
                        of {this.state.MAX_TIME}
                    </div>
                </div>
            </div>
        )
    }

    getTimeState() {
        return utils.fmtMSS(this.state.time)
    }

    getPercent() {
        return (this.state.time || 0) / (this.state.MAX_TIME || 1)
    }
    getStrokeDash() {
        return MAX_DEG * this.getPercent()
    }
    getStrokeColor() {
        return utils.percent2color(100 * this.getPercent())
    }
}