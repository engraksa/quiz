import React, { Component } from 'react'
import '../css/Home.css'
import utils from './utils'
import ajax from './ajax'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cats: [],
            pages: 1,
            pageInd: 0
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        this.props.toggleWaiting(true)
        let url = ajax.getUrl()
        ajax.get(`${url}/quiz/client/cat`, (isOK, data) => {
            this.props.toggleWaiting(false)
            if (isOK) {
                this.setState(data)
            } else {
                this.props.notifyError('Retrieve data is not okay!', 'Something wrong')
            }
        })
    }

    render() {
        return (
            <div id="home">
                <div className="minion-container minion-card">
                    <h3>Quiz with Ahladang</h3>
                    <img src={`${ajax.getUrl()}/images/announce.png`} height="140" alt="" />
                    <p>We make testing platform for you education testing</p>
                </div>
                <div className="card-container">
                    <h2 className="title">Quiz Cards</h2>
                    <ul className="flex cards">
                        {this.state.cats.map((e =>
                            <li key={e.id} className="cursor-pointer" onClick={() => {
                                this.props.onQuizCardClick && this.props.onQuizCardClick(e.id)
                            }}>
                                <h2>{e.title}</h2>
                                <div className="info">
                                    <span>question amount: {e.questionAmount},</span>
                                    <span>quiz time: {e.quizTime},</span>
                                    <span>time bonus score: {e.bonusTimeScore}</span>
                                </div>
                                <hr />
                                <p>{e.description}</p>
                            </li>))}
                    </ul>
                </div>
            </div >
        )
    }
}