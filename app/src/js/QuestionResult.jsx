import React, { Component } from 'react'
import utils from './utils'
import ajax from './ajax'

import '../css/QuestionResult.css'
import '../css/InputControl.css'

export default class QuestionResult extends Component {
    constructor(props) {
        super(props)
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    render() {
        return (
            <div className={`question-result ${this.props.ind === this.props.step ? 'show' : 'hide'}`}>
                <div className="info">
                    <div className="info-item">correct score: {this.props.correctScore.toFixed(2)}</div>
                    <div className="info-item">incorrect score: {this.props.incorrectScore}</div>
                    <div className="info-item">correct percent: {Math.floor(this.props.correctPercent)}</div>
                    <div className="info-item">incorrect percent: {Math.floor(this.props.incorrectPercent)}</div>
                    <div className="info-item">score: {this.props.score}</div>
                </div>
                <h1>{this.props.text}</h1>
                <div className="image">
                    {
                        this.props.image && <img
                            src={`${ajax.getUrl()}/${this.props.imageFolder}/${this.props.image}`}
                            alt="img"
                            onError={(event) => {
                                this.props.onImageError && this.props.onImageError(this.props.id)
                            }}
                            onLoad={(event) => {
                                this.props.onImageLoad && this.props.onImageLoad(this.props.id)
                            }} />
                    }
                </div>
                {this.genAnswer()}

            </div>
        )
    }

    genAnswer() {
        return this.props.submitAnswers.type === utils.answerType.RADIO ?
            this.props.submitAnswers.options.map((option, index) =>
                <label key={index}
                    className={`control control--radio ${option.checked ? (option.correct ? 'correct' : 'incorrect') : ''}`}
                    title={option.checked ? (option.correct ? 'correct' : 'incorrect') : ''}>
                    {option.text}
                    <input type="radio"
                        checked={option.checked} disabled />
                    <div className="control__indicator"></div>
                </label>) :
            this.props.submitAnswers.options.map((option, index) =>
                <label key={index}
                    className={`control control--checkbox ${option.checked ? (option.correct ? 'correct' : 'incorrect') : ''}`}
                    title={option.checked ? (option.correct ? 'correct' : 'incorrect') : ''}>
                    {option.text}
                    <input type="checkbox"
                        checked={option.checked} disabled />
                    <div className="control__indicator"></div>
                </label>)
    }
}