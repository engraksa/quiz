import React, { Component } from 'react'
import QuestionResult from './QuestionResult'
import Complete from './Complete'
import utils from './utils'

import '../css/ScoreBack.css'

export default class ScoreBack extends Component {
    constructor(props) {
        super(props)
        this.state = {
            score: this.updateScore(this.props),
            step: 0
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        window.scoreBack = this
    }

    componentWillReceiveProps(nextProps) {
        let score = this.updateScore(nextProps)
        this.setState({ score })
    }
    updateScore(nextProps) {
        let score = 0
        for (let i = 0; i < nextProps.result.length; i++) {
            let item = nextProps.result[i]
            score += item.score
        }
        return score
    }

    render() {
        return (
            <div id="score-back">
                <div className="overlay"></div>
                <div className="box">
                    <h3>Your Results:</h3>
                    <div className="score-info">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Score:</td>
                                    <td>{this.props.score}</td>
                                </tr>
                                <tr>
                                    <td>Time Bonus Score:</td>
                                    <td>{this.props.timeBonusScore}</td>
                                </tr>
                                <tr>
                                    <td>Total:</td>
                                    <td>{this.state.score + this.props.timeBonusScore}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className={`pass-fail ${this.props.isPass ? 'pass' : 'fail'}`}>
                    </div>
                    <div className="complete">
                        {this.props.result.length > 1 &&
                            <div>
                                <div className="step">
                                    <Complete AMOUNT={this.props.result.length}
                                        style={{ position: 'relative', opacity: 0.7 }}
                                        step={this.state.step}
                                        pointColor={"#2991d6"}
                                        contrast={true}
                                        showInfo={false}
                                        onStepClick={this._onStepClick.bind(this)} />
                                </div>
                                <div className="next-button">
                                    <button className="the-button"
                                        onClick={this._onNextClick.bind(this)}
                                        disabled={!this._isCanNext()}>next</button>
                                </div>
                            </div>
                        }
                    </div>
                    <div className="result">
                        {
                            this.props.result.map((item, ind) => {
                                let question = item.question
                                return <QuestionResult key={ind}
                                    id={question.id}
                                    text={question.text}
                                    imageFolder={this.props.imageFolder}
                                    image={question.image}
                                    correctScore={question.correctScore}
                                    incorrectScore={question.incorrectScore}
                                    submitAnswers={item.submitAnswers}
                                    correctPercent={item.correctPercent}
                                    incorrectPercent={item.incorrectPercent}
                                    score={item.score}
                                    result={item.result}
                                    ind={ind}
                                    step={this.state.step} />
                            })
                        }
                    </div>
                    <div className="buttons">
                        <button onClick={() => {
                            this.props.onHome && this.props.onHome()
                        }} className="the-button">
                            Home
                        </button>
                        <button onClick={() => {
                            this.props.nextQuiz && this.props.nextQuiz()
                        }} className="the-button">
                            Next Quiz
                        </button>
                    </div>
                </div>
            </div>
        )
    }
    _isCanNext() {
        return this.state.step < this.props.result.length - 1
    }
    _onNextClick() {
        this.setState(prevState => {
            prevState.step++;
            return prevState
        })
    }
    _onStepClick(step) {
        this.setState({ step })
    }
}