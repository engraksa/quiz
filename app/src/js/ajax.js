const ajax = {
    getUrl() {
        return window.SERVER_URL
    },
    get(url, callback = () => {}) {
        let xmlHttp = new XMLHttpRequest()
        xmlHttp.open("GET", url)
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                let data = xmlHttp.responseText
                try {
                    data = JSON.parse(data)
                } catch (e) {}
                callback(xmlHttp.status === 200, data)
            }
        }
        xmlHttp.send()
    },
    post(url, data = {}, callback = () => {}) {
        let xmlHttp = new XMLHttpRequest()
        xmlHttp.open("POST", url)
        xmlHttp.setRequestHeader("Content-Type", "application/json")
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                let data = xmlHttp.responseText
                try {
                    data = JSON.parse(data)
                } catch (e) {}
                callback(xmlHttp.status === 200, data)
            }
        }
        xmlHttp.send(JSON.stringify(data))
    }
}

export default ajax