import React, { Component } from 'react'
import Avatar from './Avatar'
import renderUsername from './renderUsername'
import '../css/Profile.css'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        if(!this.props.user) return null
        return (<div
            className="profile cursor-pointer"
            onClick={() => {
                this.props.onProfileClick && this.props.onProfileClick()
            }}
            style={this.props.style}>
            <div className="avatar-container">
                <Avatar user={this.props.user} />
            </div>
            <div className="user-info">
                <div className="username">
                    {renderUsername(this.props.user.username)}
                </div>
                <div className="profile-info">
                    <span className="quiz-count">{this.props.user.data.quiz || 0}</span>
                    <span className="pass-count">{this.props.user.data.pass || 0}</span>
                    <span className="fail-count">{this.props.user.data.fail || 0}</span>
                    <span className="score-count">{this.props.user.data.score || 0}</span>
                </div>
            </div>
        </div>
        );
    }
}

export default Profile;
