import React, { Component } from 'react'

import '../css/Avatar.css'

import utils from './utils'
import authType from './auth-types'

export default class Avatar extends Component {

    render() {
        return (
            <div
                className={`avatar ${this.props.onAvatarClicked ? 'cursor-pointer' : ''}`}
                onClick={this.props.onAvatarClicked && this.props.onAvatarClicked.bind(this)} >
                <img
                    className={`pic ${this.props.user.data.strength}`}
                    src={this.props.user.avatar || utils.getDefaultAvatar()} alt=""
                    onError={(e) => { e.target.src = utils.getDefaultAvatar() }} />
                {this.props.user.authTypeId &&
                    <img
                        className="type"
                        src={`${window.QuizPublicPath}/raw-image/avatar-${this.getAuthType(this.props.user.authTypeId)}.png`}
                        alt="ava" />
                }
            </div>
        )
    }

    getAuthType(id) {
        switch (id) {
            case authType.facebook.id:
                return 'f';
            case authType.twitter.id:
                return 't';
            case authType.googleplus.id:
                return 'g';
            default:
                return ''
        }
    }
}