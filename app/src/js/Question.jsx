import React, { Component } from 'react'
import utils from './utils'
import ajax from './ajax'

import '../css/Question.css'
import '../css/InputControl.css'

export default class Question extends Component {
    constructor(props) {
        super(props)
        this.state = {
            result: {}
        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
    }

    _callForDocumentWidth() {
    }

    render() {
        return (
            <div className={`question ${this.props.ind === this.props.step ? 'show' : 'hide'}`}>
                <div className="info">
                    <span className="info-item">correct score: {this.props.correctScore}</span>
                    <span className="info-item">incorrect score: {this.props.incorrectScore}</span>
                </div>
                <h1>{this.props.text}</h1>
                <div className="image">
                    {
                        this.props.image && <img
                            src={`${ajax.getUrl()}/${this.props.imageFolder}/${this.props.image}`}
                            alt="img"
                            onError={(event) => {
                                this.props.onImageError && this.props.onImageError(this.props.id)
                            }}
                            onLoad={(event) => {
                                this.props.onImageLoad && this.props.onImageLoad(this.props.id)
                            }} />
                    }
                </div>
                {this.genAnswer()}
            </div>
        )
    }

    genAnswer() {
        return this.props.answer.type === utils.answerType.RADIO ?
            this.props.answer.options.map((answer, index) =>
                <label key={index} className="control control--radio">{answer.text}
                    <input type="radio" value={answer.id}
                        checked={!!this.state.result[answer.id]}
                        onChange={() => {
                            this.setState(prevState => {
                                prevState.result = {}
                                prevState.result[answer.id] = true
                                return prevState
                            }, () => {
                                this.props.onAnswer({
                                    ind: index,
                                    qId: this.props.id,
                                    result: this.state.result
                                })
                            })
                        }} />
                    <div className="control__indicator"></div>
                </label>) :
            this.props.answer.options.map((answer, index) =>
                <label key={index} className="control control--checkbox">{answer.text}
                    <input type="checkbox"
                        checked={!!this.state.result[answer.id]}
                        onChange={() => {
                            this.setState(prevState => {
                                prevState.result[answer.id] = !prevState.result[answer.id]
                                return prevState
                            }, () => {
                                this.props.onAnswer({
                                    qId: this.props.id,
                                    ind: this.props.ind,
                                    result: this.state.result
                                })
                            })
                        }} />
                    <div className="control__indicator"></div>
                </label>)
    }
}