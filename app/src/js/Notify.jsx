import React, { Component } from 'react'
import ReactNotify from 'react-notify'
import '../css/Notify.css'

export default class Notify extends Component {
    constructor(props) {
        super(props)
        this.notifyTime = 6e3
    }

    error(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.error(title, message, notifyTime || this.notifyTime)
    }
    info(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.info(title, message, notifyTime || this.notifyTime)
    }
    success(title, message, notifyTime) {
        if (title instanceof Object) {
            message = title.message
            notifyTime = title.notifyTime
            title = title.title
        }
        this.notifier.success(title, message, notifyTime || this.notifyTime)
    }

    render() {
        return (
            <div id="notify">
                <ReactNotify
                    ref={(notifier) => { this.notifier = notifier }} />
            </div>
        );
    }
}