import React, { Component } from 'react'
import '../css/Waiting.css'

export default class Waiting extends Component {
    render() {
        return this.props.isWaiting ? (
            <div id="waiting-container">
                <div className="loader">
                    <svg className="circular" viewBox="25 25 50 50">
                        <circle className="path" cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10" />
                    </svg>
                </div>
            </div>
        ) : null;
    }
}