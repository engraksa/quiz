'use strict'

module.exports = {
  admin: {
    id: 1,
    name: 'Admin',
    key: 'admin'
  },
  tester: {
    id: 2,
    name: 'Tester',
    key: 'tester'
  },
  user: {
    id: 3,
    name: 'User',
    key: 'user'
  },
  getRole: function (id) {
    switch (id) {
      case this.admin.id:
        return this.admin
      case this.tester.id:
        return this.tester
      case this.user.id:
        return this.user
      default:
        return null
    }
  },
  getRoleName: function (id) {
    let role = this.getRole(id)
    return role ? role.name : 'Unknown'
  },
  getRoleKey: function (id) {
    let role = this.getRole(id)
    return role ? role.key : 'unknown'
  },
  isAdminRole: function (id) {
    return id === this.admin.id
  },
  isTesterRole: function (id) {
    return id === this.tester.id
  },
  isUserRole: function (id) {
    return id === this.user.id
  }
}