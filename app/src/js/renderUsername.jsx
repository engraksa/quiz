import React from 'react'

import utils from './utils'

export default function (username) {
    return <span className="rendered-username" style={{
        color: `#${utils.strToColorCode(username)}`
    }}>
        {username}</span>
}