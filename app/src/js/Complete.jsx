import React, { Component } from 'react'
import '../css/Complete.css'
import utils from './utils'

const WIDTH = 50

export default class Complete extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        utils.addEvent(window, "resize", this._callForDocumentWidth.bind(this))
        utils.addEvent(window, "load", this._callForDocumentWidth.bind(this))
        this.lastI = 0
        this.pointers = {}
    }

    _callForDocumentWidth() {
    }

    componentDidMount() {
        this.ensureVisible();
    }

    componentDidUpdate() {
        this.ensureVisible();
    }

    render() {
        return (
            <div id="complete" style={this.props.style || {}}>
                <progress
                    style={{
                        width: `${WIDTH * (this.props.AMOUNT - 1)}px`
                    }}
                    className={`progress-bar${this.props.contrast ? ' contrast' : ''}`}
                    min="1" max="100"
                    value={this.getPercentage()}></progress>
                {this.getAmountElement()}
                <p style={{
                    display: this.props.showInfo ? '' : 'none'
                }} className="info">{this.getPercentage()}% Complete</p>
            </div >
        )
    }

    getPercentage() {
        return Math.floor(this.props.step * 100 / (this.props.AMOUNT - 1))
    }

    getAmountElement() {
        let arr = []
        let props = this.props
        for (let i = 0; i < props.AMOUNT; i++) {
            let isReach = props.step >= i
            if (isReach) this.lastI = i + ''
            arr.push({
                className: isReach ? 'bordered' : '',
                left: `${i * WIDTH}px`
            })
        }
        let background = this.props.pointColor
        this.pointers = {}
        return arr.map((e, ind) => <span
            ref={(ele) => { this.pointers[ind + ''] = ele }}
            key={ind} className={`cursor-pointer point ${e.className}`}
            style={{
                left: e.left,
                background
            }}
            onClick={() => {
                this.props.onStepClick && this.props.onStepClick(ind)
            }} />)
    }

    ensureVisible() {
        let ele = this.pointers[this.lastI]
        ele && ele.scrollIntoView({ inline: 'nearest', block: 'nearest', behavior: 'smooth' })
    }
}