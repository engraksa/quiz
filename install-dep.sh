#!/bin/bash

./git-clean.sh

app_dir=./app
cd $app_dir
npm install
npm run build
npm run build:mobile
cd ..
cd ./app-admin
npm install
cd ..
cd ./server
npm install
cd ..
cd ./web-server
npm install
cd ..

cd $cordova_dir
git checkout ./platforms/android/build.gradle
git checkout ./platforms/ios/.gitignore
git checkout ./platforms/ios/Quiz/Quiz-Info.plist
cd ..