#!/bin/bash
quiz_prod=../quiz-prod
build_dir=./server/build
rm -r $quiz_prod/config
rm -r $quiz_prod/images
rm -r $quiz_prod/model
rm -r $quiz_prod/public
rm -r $quiz_prod/route
rm -r $quiz_prod/controller
rm -r $quiz_prod/public-admin
rm -r $quiz_prod/views
rm $quiz_prod/index.js
rm $quiz_prod/offline.js
cp -r $build_dir/** $quiz_prod
echo "copied to quiz-prod"
git -C $quiz_prod add .
DATE=$(date +"%Y-%m-%d-%H-%M")
git -C $quiz_prod commit -m "update-$DATE"
git -C $quiz_prod push origin prod