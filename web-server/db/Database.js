/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const address = require('../config/address')
const camo = require('camo')
var _onDbConnectedListeners = []
class Database {
  constructor() {
    this.isDbConnected = false
  }
  set db(db) {
    this._db = db
    this.isDbConnected = true
    Database._callListener()
  }
  get db() {
    return this._db
  }
  static _callListener() {
    let db = Database.getInstant().db
    if (!db) return
    while (_onDbConnectedListeners.length) {
      let listener = _onDbConnectedListeners.shift()
      listener(db)
    }
  }
  //* *********    Static Methods         **********
  static connectDb() {
    return new Promise((resolve, reject) => {
      camo.connect(address.CONNECTION_URL).then((db) => {
        if (!Database._instant) Database._instant = new Database()
        Database._instant.db = db
        resolve(db)
      }).catch(reject)
    })
  }
  static getInstant() {
    if (!Database._instant) Database._instant = new Database();
    !Database._instant.db && this.connectDb()
    return Database._instant
  }
  static addOnDbConnectedListener(listener) {
    _onDbConnectedListeners.push(listener)
    this._callListener()
  }
}
module.exports = Database