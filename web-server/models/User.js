/****************************************************************************
Copyright (c) 2017 Raksa Eng<eng.raksa@gmail.com>

 http://ahladang.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

'use strict'

const camo = require('camo')
const collectionNames = require('../config/collection-names')

class User extends camo.Document {
    constructor() {
        super()

        this.socialId = String
        this.authTypeId = Number
        this.roleId = Number
        this.username = String
        this.email = String
        this.password = String
        this.avatar = String
        this.token = String
        this.createdDate = Date
        this.lastUpdatedDate = Date
        this.pendingVerifyCode = String
        this.pendingResetPasswordCode = String
        this.blockedUntil = Date
    }
    static collectionName() {
        return collectionNames.user
    }
}

module.exports = User