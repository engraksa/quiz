#!/bin/bash

server_dir=./server
build_dir=$server_dir/build
public_dir=$server_dir/public
public_admin_dir=$server_dir/public-admin
app_dir=./app
app_admin_dir=./app-admin

npm run build --prefix $app_dir
rm -r $public_dir
mkdir -p $public_dir
cp -r $app_dir/build/* $public_dir
echo "copied to app"

npm run build --prefix $app_admin_dir
rm -r $public_admin_dir
mkdir -p $public_admin_dir
cp -r $app_admin_dir/build/* $public_admin_dir
echo "copied to app-admin"

rm -r $build_dir
mkdir -p $build_dir
cp -r $server_dir/images $build_dir
cp -r $server_dir/public-admin $build_dir
cp -r $server_dir/route $build_dir
cp -r $server_dir/config $build_dir
cp -r $server_dir/model $build_dir
cp -r $server_dir/views $build_dir
cp -r $server_dir/public $build_dir
cp -r $server_dir/controller $build_dir
cp $server_dir/offline.js $build_dir
cp $server_dir/index.js $build_dir
echo "copied all to build"